
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .forms import Message_Form
from .models import Subscribers
from django.utils.encoding import force_text
from django.http import HttpRequest


# Create your tests here.
class Story10UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/story10/subscribe')
        self.assertEqual(response.status_code,200)

    def test_lab10_using_subscribe_func(self):
        found = resolve('/story10/subscribe')
        self.assertEqual(found.func, myform)
    
    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = myform(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Subscribe to Nasya!', html_response)

# Create your tests here.
