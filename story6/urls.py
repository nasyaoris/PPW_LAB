from django.conf.urls import url
from .views import *
urlpatterns = [
 url(r'^status', get_status, name='get_status'),
 url(r'^mystatus', mystatus, name='mystatus' ),
 url(r'^profile', profile, name='profile' )
]