from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import profile, get_status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class Story6UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/story6/status')
        self.assertEqual(response.status_code,200)

    def test_submit_url_is_exist(self):
        response = Client().get('/story6/mystatus')
        self.assertEqual(response.status_code,302)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = get_status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?', html_response)

    def test_profile_url_is_exist(self):
        response = Client().get('/story6/profile')
        self.assertEqual(response.status_code,200)

    


# Create your tests here.
class Story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://story6-oristania.herokuapp.com/story6/status.html')
        # find the form element
        description = selenium.find_element_by_id('id_my_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        description.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

    def test_navbar_color(self):
        selenium = self.selenium
        selenium.get('https://story6-oristania.herokuapp.com/story6/status.html')
        navbar = selenium.find_element_by_tag_name('nav')
        self.assertEqual('rgba(248, 248, 248, 1)' , navbar.value_of_css_property('background-color'))

    def test_body_color(self):
        selenium = self.selenium
        selenium.get('https://story6-oristania.herokuapp.com/story6/status.html')
        body = selenium.find_element_by_tag_name('body')
        self.assertEqual('rgba(255, 255, 255, 1)' , body.value_of_css_property('background-color'))

    def test_header_position(self):
        selenium = self.selenium
        selenium.get('https://story6-oristania.herokuapp.com/story6/status.html')
        navbar = selenium.find_element_by_tag_name('nav')
        self.assertEqual({'x':0 , 'y': 0}, navbar.location)

    def test_submit_button_position(self):
        selenium = self.selenium
        selenium.get('https://story6-oristania.herokuapp.com/story6/status.html')
        button = selenium.find_element_by_tag_name('button')
        self.assertEqual({'x':0 , 'y': 166}, button.location)

    

# Create your tests here.
