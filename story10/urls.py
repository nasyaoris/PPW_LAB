from django.urls import path
from django.contrib import admin
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *

urlpatterns = [
    path('subscribe', myform, name="myForm"),
    path('validate', checkValid, name='validate'),
    path('success', submit_success, name = 'submit_success')
]
