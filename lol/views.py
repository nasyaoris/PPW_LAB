from django.shortcuts import render

def home(request):
    return render(request, "homepage.html")

def gallery(request):
    return render(request, "gallery.html")
# Create your views here.
