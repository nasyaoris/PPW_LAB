from django.conf.urls import url
from .views import *
urlpatterns = [
 url(r'^Home', index, name='index'),
 url(r'^Profile', index1, name='index1'),
 url(r'^Experience', index2, name='index2'),
 url(r'^Register', index3, name='index3'),
 url(r'^myForm', index5, name='index5'),
 url(r'^mySchedule', jadwal_respon, name='jadwal_respon' ),
 url(r'^Post', message_post, name= 'message_post'),
 url(r'^Delete', delete_all, name='delete_all')
]
