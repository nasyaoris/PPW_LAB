from django.shortcuts import render
from django.http import HttpResponse
from .models import Subscribers
from .forms import Message_Form
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


def myform(request):
    response = {}
    response['message_form'] = Message_Form
    return render(request, 'formSubscribe.html', response)

@csrf_exempt
def checkValid(request):
	email = request.POST.get("email")
	data = {'not_valid': Subscribers.objects.filter(email__iexact=email).exists()}
	return JsonResponse(data)
    
def submit_success(request):
    submit_form = Message_Form(request.POST or None)
    if(submit_form.is_valid()):
        cd = submit_form.cleaned_data
        newSubscriber = Subscribers(nama=cd['nama'], password=cd['password'], email=cd['email'])
        newSubscriber.save()
        data = {'nama': cd['nama'], 'password': cd['password'], 'email':cd['email']}
    return JsonResponse(data)



    
# Create your views here.
