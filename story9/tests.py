from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .views import data
from .views import login
from .views import logoutPage
from .views import tambah
from .views import kurang
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
# Create your tests here.
class Story9UnitTest(TestCase):

    def test_story_9_url_is_exist(self):
        response = Client().get('/story9/books')
        self.assertEqual(response.status_code,200)

    def test_story_9_hello_template(self):
        response = Client().get('/story9/books')
        self.assertTemplateUsed(response, 'book.html')
    
    def test_story_9_using_index_func(self):
        found = resolve('/story9/books')
        self.assertEqual(found.func, index)
    
    def test_story_9_using_data_func(self):
        found = resolve('/story9/data/')
        self.assertEqual(found.func, data)

    def test_story_9_using_login_func(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, login)

    def test_story_9_login_is_exist(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code,200)

    def test_story_9_using_logout_func(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, logoutPage)

    def test_story_9_using_tambah_func(self):
        found = resolve('/story9/tambah/')
        self.assertEqual(found.func, tambah)

    def test_story_9_using_kurang_func(self):
        found = resolve('/story9/kurang/')
        self.assertEqual(found.func, kurang)
        
    def test_story_9_login_template(self):
        response = Client().get('/story9/login')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_story_9_login_google(self):
        request = HttpRequest()
        response = login(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Login with google", html_response)
        
    def test_name_title(self):
        response = Client().get('/story9/login')
        homepage_title = response.content.decode('utf8')
        self.assertIn('Login or Register', homepage_title)
        
    def test_name_books_title(self):
        response = Client().get('/story9/books')
        homepage_title = response.content.decode('utf8')
        self.assertIn('Books', homepage_title)

    



    

    


# Create your tests here.
