from django.conf.urls import url
from .views import home
from .views import gallery


urlpatterns = [
    url(r'^$', home, name = "Home"),
    url(r'^gallery', gallery, name = "Gallery")
]