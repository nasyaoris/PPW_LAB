from django import forms

class Message_Form(forms.Form):
    name_attrs = {
		'type' : 'text',
		'class' : 'form-control',
		'placeholder' : 'Full Name',
	}
    email_attrs = {
		'type' : 'text',
		'class' : 'form-control',
		'placeholder' : 'Example: abcde@gmail.com'
	}
    pass_attrs = {
		'type' : 'password',
		'class' : 'form-control',
		'placeholder' : 'Password'
	}
    
    nama = forms.CharField(max_length=100, widget = forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(widget = forms.TextInput(attrs=email_attrs))
    password = forms.CharField(max_length=20, widget = forms.PasswordInput(attrs=pass_attrs))


