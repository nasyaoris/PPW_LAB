from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import story8
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Story8UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/story8/story8')
        self.assertEqual(response.status_code,200)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = story8(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Oristania Wahyu Nabasya', html_response)

    def test_there_is_button(self):
        request = HttpRequest()
        response = story8(request)
        html_response = response.content.decode("utf8")
        self.assertIn("Change Theme" ,html_response)

    
        

    
# Create your tests here.
