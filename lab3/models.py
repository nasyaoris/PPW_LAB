from django.db import models
from django.utils import timezone
from datetime import datetime, date

class JadwalPribadi(models.Model):
    hari = models.DateField()
    tanggal = models.DateTimeField()
    nama_kegiatan = models.CharField(max_length=30)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=15)
    
# Create your models here.
