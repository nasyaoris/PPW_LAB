$('.ml11 .letters').each(function(){
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letters'>$&</span>"));
  });

$(document).on('click', '#search', function(){
    $(body).empty();
    var searchbar = $("input").val();
})
  

    $.ajax({
      url: "{% url 'databooks' %}",
      datatype: 'json',
      success: function(result){
          console.log("test");
          var books_object = jQuery.parseJSON(result);
          console.log(books_object);
          renderHTML(books_object);
      }
    });
  
  
  function renderHTML(data){
      console.log("render");
      var books_list = data.items;
            for (var i = 0; i < books_list.length; i++){
              var title = books_list[i].volumeInfo.title;
              var author = books_list[i].volumeInfo.authors[0];
              var published = books_list[i].volumeInfo.publishedDate;
              var temp = title.replace(/'/g, "\\'");
              var table = 
              '<tr class="table-books">'+
              '<td class = "nomor">' + (i+1) + '</td>' +
              '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
              '<td class="title">'+title+'</td>'+
              '<td class= "author">'+author+'</td>'+
              '<td class= "published">' + published+'</td>'+
              '<td class= "favourite">' + '<button id = "star" ><i class = "fas fa-star"</i></button>' + '</td>';
              $('tbody').append(table);
            }
    }
  
  
  
  var counter = 0;
  
  $(function(){
    $(document).on('click', '#star', function(){
      if($(this).hasClass('favorited')){
        counter -= 1;
        $(this).removeClass('favorited');
        $(this).css("color","white");
      }
  
      else{
        counter +=1;
        $(this).addClass('favorited');
        $(this).css("color", "#631731");
      }
  
      $(".numbers").html(counter);
    });
  });
  