
$(document).ready(function(){

    var myVar;

    function myFunction() {
        myVar = setTimeout(showPage, 3000);
    }
    myFunction();

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }

    $("#accordion").accordion();

    var color = ["#f8a5c2", "#F8EFBA"];
    var i = 0;

    $("button").click(function(){
        i = i < color.length ? ++i : 0;
        document.querySelector("body").style.background = color[i];
    })

   

});
