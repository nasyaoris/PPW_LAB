from django.conf.urls import url , include
from django.urls import path
from django.contrib import admin
from django.contrib.auth import views
from .views import index
from .views import data
from .views import login
from .views import tambah
from .views import kurang
from .views import logoutPage


urlpatterns = [
    url('books', index, name = "Books"),
    url('data', data, name = "databooks"),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url('login', login , name = "login"),
    url('logout', logoutPage, name='logout'),
	url('tambah', tambah, name='tambah'),
	url('kurang', kurang, name='kurang')
]
