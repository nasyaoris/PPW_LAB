from django.db import models
from django.utils.timezone import now

class Status(models.Model):
    my_status = models.CharField(max_length = 300)
    date = models.DateTimeField(default = now)
# Create your models here.
