from django import forms
attrs = {
        'class': 'form-control'
    }

class Message_Form(forms.Form):
    nama_kegiatan = forms.CharField(label= "Nama Acara")
    hari = forms.DateField(label='Hari', widget=forms.DateInput())
    tanggal = forms.DateTimeField(label='Waktu Kegiatan', widget=forms.TimeInput())
    tempat = forms.CharField(label = "Tempat Kegiatan")
    kategori = forms.CharField(label = "Kategori")
