from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect
import json
import requests

def data(request):
	try:
		search = request.get["search"]
	except:
		search = "quilting"
	
	getBooksJson = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
	jsonParsed = json.dumps(getBooksJson.json())
	return HttpResponse(jsonParsed)


def login(request):
	return render(request, "login.html")

def index(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		request.session.get('counter',0)
		print(dict(request.session))
		for key, value in request.session.items():
			print('{} => {}'.format(key,value))
	return render(request,'book.html')

def logoutPage(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/story9/login/')

def tambah(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')

def kurang(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')



# Create your views here.
