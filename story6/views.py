from django.shortcuts import render
from .models import Status
from .forms import Message_Form
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

def profile(request):
    return render(request, "profile.html")

def get_status(request):
    response = {'form': Message_Form, 'stat': Status.objects.all()}
    return render(request, 'status.html', response)

def mystatus(request):
    if request.method == 'POST':
        form = Message_Form(request.POST)
        obj = Status()
        obj.my_status = request.POST['my_status']
        obj.save()

        return HttpResponseRedirect('status')
    else:
        form = Message_Form()
    return HttpResponseRedirect('status')
        

       
# Create your views here.
