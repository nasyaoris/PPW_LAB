from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import Message_Form
from .models import JadwalPribadi
from django.http import HttpResponse
import pytz
from datetime import datetime

def index(request):
    return render(request, "Home.html")

def index1(request):
    return render(request, "Profile.html")

def index2(request):
    return render(request, "Experience.html")

def index3(request):
    return render(request, "Register.html")

def index4(request):
    return render(request, "Navbar.html")


def index5(request):
    response = {}
    response['message_form'] = Message_Form
    jadwal = JadwalPribadi.objects.all()
    response['jadwal'] = jadwal
    return render(request, 'myForm.html', response)

def message_post(request):
    response = {}
    form = Message_Form(request.POST or None)
    if(request.method == "POST"):
        response["nama_kegiatan"] = request.POST["nama_kegiatan"]
        response["hari"] = datetime.strptime(request.POST["hari"], '%Y-%m-%d')
        response["tanggal"] = datetime.strptime(request.POST["tanggal"], '%H:%M')
        response["tempat"] = request.POST["tempat"]
        response["kategori"] = request.POST["kategori"]

        jadwal = JadwalPribadi(nama_kegiatan = response["nama_kegiatan"], hari = response["hari"], tanggal = response["tanggal"], tempat = response["tempat"], kategori = response["kategori"])
        jadwal.save()
        response['jadwal'] = JadwalPribadi.objects.all().values()
        html = "mySchedule.html"
        return render(request, html, response)
        
    else:
        return HttpResponseRedirect('myForm')
    
def jadwal_respon(request):
    response = {}
    jadwal = JadwalPribadi.objects.all()
    response['jadwal_respon'] = jadwal
    return render(request, "mySchedule.html", response)

def delete_all(request):
    delete = JadwalPribadi.objects.all().delete()
    return HttpResponseRedirect('mySchedule')

# Create your views here.
